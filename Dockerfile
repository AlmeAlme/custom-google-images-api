FROM phpswoole/swoole:4.6.1-php8.0
RUN apt-get update && apt-get install -y \
	wget
RUN apt-get autoremove -y && rm -rf /var/lib/apt/lists/*
RUN wget https://github.com/Yelp/dumb-init/releases/download/v1.2.4/dumb-init_1.2.4_amd64.deb \
	&& dpkg -i dumb-init_1.2.4_amd64.deb \
	&& rm -f dumb-init_1.2.4_amd64.deb
RUN curl -sL https://deb.nodesource.com/setup_15.x | bash - \
	&& apt-get install -y nodejs
WORKDIR "/app"
ENTRYPOINT ["/usr/bin/dumb-init", "--", "php"]