#!/usr/bin/env php
<?php

declare(strict_types=1);
error_reporting(E_ALL);

require __DIR__ . '/vendor/autoload.php';

use Google\Grabber;
use Google\Parser;
use Proxy\Manager as ProxyManager;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;

$ProxyManager = ProxyManager::init();
$server = new Server("0.0.0.0", 9501);
$server->set([
	'worker_num' => 1,
	'task_worker_num' => 100,
]);

$server->on(
	"start",
	function (Server $http) use ($ProxyManager) {
		echo "Swoole HTTP server is started.\n";
		echo "Proxy count: " . $ProxyManager->count() . "\n";
	}
);
$server->on(
	"request",
	function (Request $request, Response $response) use ($server, $ProxyManager) {
		$time = microtime(true);
		if (!isset($request->get['q'])) {
			$response->end(json_encode(['status' => 0]));
			return;
		}
		for ($i = 0; $i < 100; $i++) {
			$tasks[] = [
				'query' => urldecode($request->get['q']),
				'nextPage' => isset($request->get['next']) ? base64_decode($request->get['next']) : null,
				'sid' => $request->get['sid'] ?? null,
				'proxy' => $ProxyManager->getRand()
			];
		}
		$result = $server->taskCo($tasks, 2.5);
		foreach ($result as $content) {
			if ($content !== false && $content['response'] !== null) break;
		}
		if ($content === false || $content['response'] === null) { // try without proxy
			$taskWithoutProxy = $tasks[0];
			$taskWithoutProxy['proxy'] = null;
			$result = $server->taskCo([
				$taskWithoutProxy
			], 2.5);
			$content = $result[0];
			$proxyOff = true;
		}
		if ($content['response'] === null) {
			$response->end(json_encode(['status' => 0, 'error' => 'Can\'t understand Google content']));
			return;
		}
		try {
			$parser = new Parser();
			if (!isset($request->get['next'])) {
				$result = $parser->fromFirstPageString($content['response']);
				$sid = $parser->parseSid($content['response']);
			} else {
				$result = $parser->fromFrontendDataString($content['response']);
				$sid = $tasks[0]['sid'];
			}
		} catch (\Exception $e) {
			$response->end(json_encode(['status' => 0, 'error' => $e->getMessage()]));
			return;
		}

		$response->end(json_encode([
			'status' => 1,
			'execute_time' => microtime(true) - $time,
			'proxy' => isset($proxyOff) ? 'off' : 'on',
			'gSid' => $sid,
			'gNextPage' => base64_encode($parser->getNextPage()),
			'data' => $result->getData(),
		], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
	}
);
$server->on('Task', function (Swoole\Server $server, $task_id, $worker_id, $data) {
	$grabber = new Grabber($data['query'], $data['proxy'] ?? null);
	if ($data['nextPage'] && $data['sid'])
		$data['response'] = $grabber->page($data['nextPage'], $data['sid'])->run('HoAMBc');
	else
		$data['response'] = $grabber->run('schema.org/SearchResultsPage');
	return $data;
});

$server->start();
