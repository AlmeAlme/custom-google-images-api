### Install
`./install.sh`

### Run service
`./start.sh`

### Usage
HTTP request (or open in Browser)

`http://localhost:9501/?q=[GOOGLE_IMAGES_URL_ENCODED]`

e.g. `http://localhost:9501/?q=https%3A%2F%2Fwww.google.com%2Fsearch%3Fq%3Dsmile%26hl%3Den%26tbm%3Disch` for `https://www.google.com/search?q=smile&hl=en&tbm=isch`

Query params

`q` - encoded google URL (may include google filter params `tbs` and `hl`)

`sid`  - google session ID (must be included for query next 100 results)

`next` - base64 decoded next page ID

