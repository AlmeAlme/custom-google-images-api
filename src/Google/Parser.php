<?php

namespace Google;

use Google\Exceptions\ParseDataException;

class Parser
{
	private $data;
	private $nextPage;

	public function fromFirstPageString(string $str): self
	{
		preg_match_all("!AF_initDataCallback\((.*?)\);!si", $str, $m);
		if (!isset($m[1][1]))
			throw new ParseDataException("Google javascript AF_initDataCallback() parse error", 1);
		file_put_contents($tmpfile = tempnam(sys_get_temp_dir(), 'js'), 'JSON.stringify(' . $m[1][1] . ')');
		$resultJSON = $this->execNode($tmpfile);
		unlink($tmpfile);
		if (!is_array($dataParsed = json_decode($resultJSON, true)))
			throw new ParseDataException("Google data parse error", 1);
		$this->data = $this->filter($dataParsed['data']);
		$this->nextPage = $this->parseNextPage($dataParsed['data']);
		return $this;
	}

	public function fromFrontendDataString(string $str): self
	{
		preg_match("!(\"\[.+\][\\\]?n?\")!si", $str, $m);
		if (!isset($m[1]))
			throw new ParseDataException("Google frontend data parse error", 1);
		file_put_contents($tmpfile = tempnam(sys_get_temp_dir(), 'js'), 'JSON.stringify(JSON.parse(' . $m[1] . '))');
		$resultJSON = $this->execNode($tmpfile);
		unlink($tmpfile);
		if (!is_array($dataParsed = json_decode($resultJSON, true)))
			throw new ParseDataException("Google data parse error", 1);
		$this->data = $this->filter($dataParsed);
		$this->nextPage = $this->parseNextPage($dataParsed);
		return $this;
	}

	public function parseSid(string $str): string
	{
		preg_match('!"FdrFJe"\:"([^\"]+)"!si', $str, $m);
		if (!isset($m[1])) throw new ParseDataException("SID parse error", 1);
		return $m[1];
	}

	public function parseNextPage(array $rawGoogleData): string
	{
		return '\"' . $rawGoogleData[49] . '\",\"' . $rawGoogleData[31][0][12][16][3] . '\",\"' . $rawGoogleData[31][0][12][16][4] . '\"';
	}

	public function filter(array $rawGoogleData)
	{
		foreach ($rawGoogleData[31][0][12][2] as $d) {
			if (empty($d[1][2])) continue;
			$resultMap = [
				'thumb' => $d[1][2],
				'image' => $d[1][3],
				'page' => [
					'title' => $d[1][9][2003][3] ?? ($d[1][11][2003][3] ?? ''),
					'url' => $d[1][9][2003][2] ?? ($d[1][11][2003][2] ?? ''),
					'text' => $d[1][9][2003][12] ?? ($d[1][11][2003][12] ?? '')
				],
				'license' => $d[1][9][2006][11][1]??'',
				'caption' => $d[1][9][2008][1] ?? ''
			];
			$resultFiltered[] = $resultMap;
		}
		return $resultFiltered;
	}
	/**
	 * Execute JavaScript code via Nodejs to get json stringify data from JS vars
	 *
	 * @param string $jsFilename
	 * @return string [JSON encoded string]
	 */
	public function execNode(string $jsFilename): ?string
	{
		return shell_exec('node -p < ' . $jsFilename);
	}

	public function json()
	{
		return json_encode($this->data, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * Get the value of data
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Get the value of nextPage
	 */
	public function getNextPage()
	{
		return $this->nextPage;
	}
}
