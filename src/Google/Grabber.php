<?php

namespace Google;

class Grabber
{
	public static $debug = false;
	private $url;
	private $proxy;
	private $postData;
	private $keyword;

	public function __construct(string $url, ?array $proxy = null)
	{
		$this->url = $url;
		$this->proxy = $proxy;
		parse_str(parse_url($url, PHP_URL_QUERY), $query);
		$this->keyword = $query['q'] ?? '';
		$this->hl = $query['hl'] ?? 'en';
		$this->tbs = $query['tbs'] ?? '';
	}

	public function page(string $nextPage, string $sid): self
	{
		$this->url = 'https://www.google.com/_/VisualFrontendUi/data/batchexecute?rpcids=HoAMBc&f.sid=' . $sid;
		$this->postData =
			'f.req=[[["HoAMBc","[null,null,[null,null,null,null,null,[],[],[],null,null,null,null,null,[]],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,' .
			'[\"' . $this->keyword . '\",\"' . $this->hl . '\"' .
			($this->tbs ? ',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,\"' . $this->tbs . '\"' : '') . '],null,null,null,null,null,null,null,null,[' . $nextPage . ']]",null,"generic"]]]';
		return $this;
	}

	public function setPostData(array $postData)
	{
		$this->postData = $postData;
	}

	public function run(?string $validateString = null): ?string
	{
		$response = $this->request($this->url, $this->postData, $this->proxy);
		return $validateString === null ? $response : (strstr($response, $validateString) ? $response : null);
	}

	public function request(string $requestURL, $postData = null, ?array $proxy = null): ?string
	{
		$opt = $proxy !== null ? [
			CURLOPT_PROXY => $proxy[0],
			CURLOPT_PROXYTYPE => $proxy[1]
		] : [];
		if ($postData) $opt[CURLOPT_POSTFIELDS] = $postData;
		$response = $this->curlRequest(
			$requestURL,
			$opt
		);
		if (self::$debug) echo "$requestURL -> " . ($proxy !== null ? " {$proxy[0]}|{$proxy[1]}" : "") . "[{$response['status']}]\n";
		if ($response['status'] != 200)
			return null;
		return $response['response'];
	}

	private function curlRequest(string $url, array $aopt = []): array
	{
		$opt = [
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/100.0.1469.0 Safari/537.36",
			CURLOPT_TIMEOUT => 10
		];
		foreach ($aopt as $key => $val) {
			$opt[$key] = $val;
		}
		$ch = curl_init($url);
		curl_setopt_array($ch, $opt);
		$response = curl_exec($ch);
		$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return [
			'status' => $httpStatus,
			'response' => $response
		];
	}

	/**
	 * Get the value of keyword
	 */
	public function getKeyword()
	{
		return $this->keyword;
	}

	/**
	 * Set the value of keyword
	 *
	 * @return  self
	 */
	public function setKeyword($keyword)
	{
		$this->keyword = $keyword;

		return $this;
	}
}
