<?php

namespace Proxy\Exceptions;

use Exception;

class ProxyReadfileException extends Exception
{
}
