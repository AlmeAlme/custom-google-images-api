<?php

namespace Proxy\Exceptions;

use Exception;

class ProxyTypedFormatException extends Exception
{
}
