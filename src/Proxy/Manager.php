<?php

namespace Proxy;

use Proxy\Exceptions\ProxyReadfileException;
use Proxy\Exceptions\ProxyTypedFormatException;
use DateTime;

class Manager
{

	const DEFAULT_LIST = 'http://31.42.188.7:8080/free-proxy/all.txt?ability=0.5'; // ip:port|type
	//const DEFAULT_LIST = 'http://31.42.188.7:8080/free-proxy/google.txt';
	private static $instance = null;
	private $list = [];
	private $updateTime;
	private $pathname;
	private $renewable = true;

	private function __construct(?string $proxylist = null)
	{
		if ($proxylist) {
			if (!preg_match('!^https?\:\/\/!', $proxylist))
				if (!file_exists($proxylist))
					throw new ProxyReadfileException("[$proxylist] does not exist");
				else
					$this->renewable = false;
		} else
			$proxylist = self::DEFAULT_LIST;
		$this->pathname = $proxylist;
		$this->loadProxy($proxylist);
		self::$instance = $this;
	}

	static function init()
	{
		if (self::$instance === null) new self(...func_get_args());
		return self::$instance;
	}

	private function loadProxy(string $pathname)
	{
		foreach (file($pathname) as $line) {
			$line = trim($line);
			$proxy = $this->getTypedProxy($line);
			$proxiesUniq[$line] = $proxy;
		}
		$this->list = isset($proxiesUniq) ? array_values($proxiesUniq) : [];
		$this->updateTime = new DateTime();
	}

	public function renew()
	{
		if (!$this->renewable) return;
		$this->loadProxy($this->pathname);
	}

	private function getTypedProxy(string $line): array
	{
		$proxy = explode("|", $line);
		if (!isset($proxy[1])) $proxy[1] = 'http';
		$proxyMap2CURL = [
			'http' => CURLPROXY_HTTP,
			'socks4' => CURLPROXY_SOCKS4,
			'socks5' => CURLPROXY_SOCKS5
		];
		if (!isset($proxyMap2CURL[$proxy[1]]))
			throw new ProxyTypedFormatException("Unrecognized proxy type [{$proxy[1]}]");

		$proxy[1] = $proxyMap2CURL[$proxy[1]];
		return $proxy;
	}

	/**
	 * Get the value of updateTime
	 */
	public function getUpdateTime()
	{
		return $this->updateTime;
	}

	public function count()
	{
		return count($this->list);
	}

	public function getRand(): ?array
	{
		if (!$c = count($this->list)) return null;
		mt_srand(); // always needed into swoole subprocess
		return $this->list[mt_rand(0, $c - 1)];
	}
}
